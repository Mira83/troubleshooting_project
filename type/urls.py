from django.urls import path
from type import views

urlpatterns = [
    path('add_type/', views.TypeCreateView.as_view(), name='new_type'),
    path('list_type/', views.TypeListView.as_view(), name='list_type'),
    path('update_type/ <int:pk>', views.TypeUpdateView.as_view(), name='update_type'),
    path('delete_type/<int:pk>', views.TypeDeleteView.as_view(), name='delete_type'),
    path('detail_type/<int:pk>', views.TypeDetailView.as_view(), name='detail_type'),
    path('type/<int:pk>', views.get_products_per_type, name='type'),
]
