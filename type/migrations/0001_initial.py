# Generated by Django 3.2.6 on 2021-08-02 16:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('products', '0002_products_brand'),
    ]

    operations = [
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(max_length=100)),
                ('products', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='products.products')),
            ],
        ),
    ]
