from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from ticket.forms import TicketForm
from ticket.models import Ticket


class TicketCreateView(CreateView):
    template_name = 'ticket/create_ticket.html'
    model = Ticket
    form_class = TicketForm
    # fields = '__all__'
    success_url = reverse_lazy('create_ticket')


class TicketListView(ListView):
    template_name = 'ticket/list_ticket.html'
    model = Ticket
    context_object_name = 'tickets'


class TicketUpdateView(UpdateView):
    template_name = 'ticket/update_ticket.html'
    model = Ticket
    context_object_name = 'tickets'
    fields = '__all__'
    success_url = reverse_lazy('list_ticket')


class TicketDeleteView(DeleteView):
    template_name = 'ticket/delete_ticket.html'
    model = Ticket
    context_object_name = 'delete_ticket'
    success_url = reverse_lazy('list_ticket')


class TicketDetailView(DetailView):
    template_name = 'ticket/detail_ticket.html'
    model = Ticket
    context_object_name = 'detail_ticket'
