from brand.models import Brand


def get_all_brands(request):
    all_brands = Brand.objects.all()
    return {"all_brands": all_brands}
