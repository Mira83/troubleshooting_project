from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from information.models import Information


class InfoCreateView(CreateView):
    template_name = 'information/add_info.html'
    model = Information
    fields = '__all__'
    # form_class = ProductsForm
    context_object_name = 'information'
    success_url = reverse_lazy('new_information')


class InfoListView(ListView):
    template_name = 'information/list_info.html'
    model = Information
    context_object_name = 'informations'


class InfoUpdateView(UpdateView):
    template_name = 'information/update_info.html'
    model = Information
    context_object_name = 'product'
    fields = '__all__'
    success_url = reverse_lazy('list_product')


class InfoDeleteView(DeleteView):
    template_name = 'information/delete_info.html'
    model = Information
    context_object_name = 'delete_info'
    success_url = reverse_lazy('list_info')


class InfoDetailView(DetailView):
    template_name = 'information/detail_info.html'
    model = Information
    context_object_name = 'detail_info'


def get_type_per_info(request, pk):
    all_type_per_info = Information.objects.filter(type_id=pk)
    context = {
        'all_type_per_info': all_type_per_info
    }
    return render(request, 'home/all_info_per_products.html', context)
