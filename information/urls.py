from django.urls import path
from information import views

urlpatterns = [
    path('information/', views.InfoCreateView.as_view(), name='new_information'),
    path('list_information/', views.InfoListView.as_view(), name='list_information'),
    path('update_information/ <int:pk>', views.InfoUpdateView.as_view(), name='update_information'),
    path('delete_information/<int:pk>', views.InfoDeleteView.as_view(), name='delete_information'),
    path('detail_information/<int:pk>', views.InfoDetailView.as_view(), name='detail_information'),
    path('info/<int:pk>', views.get_type_per_info, name='info'),
]
