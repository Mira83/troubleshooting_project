from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView
from brand.forms import BrandForm
from brand.models import Brand


class BrandCreateView(CreateView):
    template_name = 'brand/add_brand.html'
    model = Brand
    # fields = '__all__'
    form_class = BrandForm
    context_object_name = 'brand'
    success_url = reverse_lazy('new_brand')


class BrandListView(ListView):
    template_name = 'brand/list_brand.html'
    model = Brand
    context_object_name = 'brands'


class BrandUpdateView(UpdateView):
    template_name = 'brand/update_brand.html'
    model = Brand
    context_object_name = 'brand'
    fields = '__all__'
    success_url = reverse_lazy('list_brand')


class BrandDeleteView(DeleteView):
    template_name = 'brand/delete_brand.html'
    model = Brand
    context_object_name = 'delete_manufacturer'
    success_url = reverse_lazy('list_brand')


class BrandDetailView(DetailView):
    template_name = 'brand/detail_brand.html'
    model = Brand
    context_object_name = 'detail_brand'
