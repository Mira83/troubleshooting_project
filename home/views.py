from django.shortcuts import render
from django.views.generic import TemplateView
from products.models import Products


class HomeTemplateView(TemplateView):
    template_name = 'home/template_name.html'


def counter(request):
    visit_count = request.session.get('counter', 0)
    request.session['counter'] = visit_count + 1
    context = {
        'count': visit_count
    }
    return render(request, 'home/counter.html', context)


def get_products_per_brand(request, pk):
    all_products_per_brand = Products.objects.filter(brand_id=pk)
    context = {
        'all_products_per_brand': all_products_per_brand
    }
    return render(request, 'home/all_products_per_brand.html', context)
